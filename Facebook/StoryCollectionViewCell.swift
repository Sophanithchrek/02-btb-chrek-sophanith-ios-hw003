//
//  StoryCollectionViewCell.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 30/11/20.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userProfileBackgroundImageView: UIImageView!
    @IBOutlet weak var userProfileStoryImageView: UIImageView!
    @IBOutlet weak var lblUserNameStory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
