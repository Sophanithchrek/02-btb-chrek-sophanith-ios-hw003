//
//  SectionTableViewCell.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 27/11/20.
//

import UIKit

class SectionTableViewCell: UITableViewCell {

    @IBOutlet weak var userProfilePostImageView: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblCaption: UILabel!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var lblAmountLike: UILabel!
    @IBOutlet weak var lblAmountComment: UILabel!
    @IBOutlet weak var lblAmountShare: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
