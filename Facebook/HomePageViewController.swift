//
//  HomePageViewController.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 27/11/20.
//

import UIKit

class HomePageViewController: UIViewController {

    @IBOutlet weak var userProfileHomeImageView: UIImageView!
    @IBOutlet weak var homePageTableView: UITableView!
    @IBOutlet weak var storyCollectionView: UICollectionView!
    
    // Create and assign data into struct -> User
    var userData = [User]()
    var user1 = User(username: "Cristiano Ronaldo", profile: UIImage(named: "cr7")!)
    var user2 = User(username: "Leo Messi", profile: UIImage(named: "leo-messi-profile")!)
    var user3 = User(username: "Neymar Jr.", profile: UIImage(named: "neymar-jr-profile")!)
    
    // Create and assign data into struct -> Post
    var postData = [Post]()
    var post1 = Post(user: "Cristiano Ronaldo", caption: "See you tomorrow Uefa Nations League! 🏴🏳️💪🏽 #finoallafine", postDetail: "cr7-post-2", amountLike: "1.1M likes", amountComment: "18K comments", amountShare: "44K shares")
    var post2 = Post(user: "Leo Messi", caption: "Diego, all the strength in the world. My family and I want to see you good asap. A hug from the heart!", postDetail: "no-image", amountLike: "1.1M likes", amountComment: "24K comments", amountShare: "11K shares")
    var post3 = Post(user: "Neymar Jr.", caption: "May God bless and protect us 🙏🏽⚽️", postDetail: "neymar-jr-post01", amountLike: "980K likes", amountComment: "9K comments", amountShare: "16K shares")
    
    // Create and assign data into struct -> Story
    var userStoryData = [Story]()
    var story1 = Story(username: "Cristiano\nRonaldo", imageStory: UIImage(named: "cr7")!, backgroundImageStory: UIImage(named: "rsz_1cr7-story-1")!)
    var story2 = Story(username: "Leo Messi", imageStory: UIImage(named: "leo-messi-profile")!, backgroundImageStory: UIImage(named: "rsz_1leo-messi-story")!)
    var story3 = Story(username: "Neymar Jr.", imageStory: UIImage(named: "neymar-jr-profile")!, backgroundImageStory: UIImage(named: "rsz_neymar-jr-story")!)
    
    var story4 = Story(username: "Cristiano\nRonaldo", imageStory: UIImage(named: "cr7")!, backgroundImageStory: UIImage(named: "rsz_1cr7-story-1")!)
    var story5 = Story(username: "Leo Messi", imageStory: UIImage(named: "leo-messi-profile")!, backgroundImageStory: UIImage(named: "rsz_1leo-messi-story")!)
    var story6 = Story(username: "Neymar Jr.", imageStory: UIImage(named: "neymar-jr-profile")!, backgroundImageStory: UIImage(named: "rsz_neymar-jr-story")!)
    
    var story7 = Story(username: "Cristiano\nRonaldo", imageStory: UIImage(named: "cr7")!, backgroundImageStory: UIImage(named: "rsz_1cr7-story-1")!)
    var story8 = Story(username: "Leo Messi", imageStory: UIImage(named: "leo-messi-profile")!, backgroundImageStory: UIImage(named: "rsz_1leo-messi-story")!)
    var story9 = Story(username: "Neymar Jr.", imageStory: UIImage(named: "neymar-jr-profile")!, backgroundImageStory: UIImage(named: "rsz_neymar-jr-story")!)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove navigationController line
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Set tint color to navigationBar
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        // Register to UINib of UITableViewCell -> SectionTableViewCell
        homePageTableView.register(UINib(nibName: "SectionTableViewCell", bundle: nil), forCellReuseIdentifier: "myCell")
        
        // Register to UINib of UITableViewCell -> StatusUITableViewCell
        homePageTableView.register(UINib(nibName: "StatusTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        // Register to UINib of UICollectionViewCell -> StoryCollectionViewCell
        storyCollectionView.register(UINib(nibName: "StoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "viewCell")
        
        // Append data into userData
        userData.append(contentsOf: [user1, user2, user3])
        
        // Append data into postData
        postData.append(contentsOf: [post1, post2, post3])
        
        print("User", userData.count)
        print("Post", postData.count)
        
        // Append data into storyData
        userStoryData.append(contentsOf: [story1, story2, story3, story4, story5, story6, story7, story8, story9])
        
    }

}

extension HomePageViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let myCell = homePageTableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! SectionTableViewCell
        
        let statusCell = homePageTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StatusTableViewCell
        
        let posts = postData[indexPath.row].postDetail

        if let data = UIImage(named: posts) {
            myCell.lblUsername.text = userData[indexPath.row].username
            myCell.userProfilePostImageView.image = userData[indexPath.row].profile
            myCell.lblCaption.text = postData[indexPath.row].caption
            myCell.postImageView.image = data
            myCell.lblAmountLike.text = postData[indexPath.row].amountLike
            myCell.lblAmountComment.text = postData[indexPath.row].amountComment
            myCell.lblAmountShare.text = postData[indexPath.row].amountShare
            return myCell
        } else {
            statusCell.lblUsername.text = userData[indexPath.row].username
            statusCell.userProfilePostImageView.image = userData[indexPath.row].profile
            statusCell.lblCaption.text = postData[indexPath.row].caption
            statusCell.lblAmountLike.text = postData[indexPath.row].amountLike
            statusCell.lblAmountComment.text = postData[indexPath.row].amountComment
            statusCell.lblAmountShare.text = postData[indexPath.row].amountShare
            return statusCell
        }
        
    }

}

extension HomePageViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userStoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let viewCell = storyCollectionView.dequeueReusableCell(withReuseIdentifier: "viewCell", for: indexPath) as! StoryCollectionViewCell
        viewCell.userProfileStoryImageView.image = userStoryData[indexPath.row].imageStory
        viewCell.userProfileBackgroundImageView.image = userStoryData[indexPath.row].backgroundImageStory
        viewCell.lblUserNameStory.text = userStoryData[indexPath.row].username
        
        return viewCell
    }

}
