//
//  User.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 1/12/20.
//

import Foundation
import UIKit

struct User {
    var username: String
    var profile: UIImage
}
