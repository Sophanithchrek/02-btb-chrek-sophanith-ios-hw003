//
//  Post.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 1/12/20.
//

import Foundation
import UIKit

struct Post {
    var user: String
    var caption: String
    var postDetail: String
    var amountLike: String
    var amountComment: String
    var amountShare: String
}
