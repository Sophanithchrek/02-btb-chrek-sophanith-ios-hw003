//
//  File.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 1/12/20.
//

import Foundation
import UIKit

struct Post {
    var user: User
    var caption: String
    var image: UIImage
    var amountLike: Int
    var amountComment: Int
    var amountShare: Int
}
