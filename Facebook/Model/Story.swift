//
//  Story.swift
//  Facebook
//
//  Created by SOPHANITH CHREK on 1/12/20.
//

import Foundation
import UIKit

struct Story {
    var username: String
    var imageStory: UIImage
    var backgroundImageStory: UIImage
}
